# How to use: Grenade Scripts   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.grenadescripts":"https://gitlab.com/eloistree/2019_07_22_grenadescripts.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.grenadescripts",                              
  "displayName": "Grenade Scripts",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Quick Scripts that your drag and drop in your project to execute a specific code when you start.",                         
  "keywords": ["Script","Tool"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    