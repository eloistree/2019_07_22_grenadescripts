﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class LightPackageManager : MonoBehaviour
{
    private static void WhatToDoWhenDrop()
    {

        Debug.Log("Hello");
        string packageFile = GetPackageFile();
        File.WriteAllText(Application.dataPath + "/../Packages/manifest.json", packageFile);
        AssetDatabase.Refresh();

    }


    public static PackageValue[] m_wantedDependencies = new PackageValue[] {
    new PackageValue("com.unity.package-manager-ui", "2.1.2"),
    new PackageValue("com.unity.modules.ai", "1.0.0"),
    new PackageValue("com.unity.modules.animation", "1.0.0"),
    new PackageValue("com.unity.modules.assetbundle", "1.0.0"),
    new PackageValue("com.unity.modules.audio", "1.0.0"),
    new PackageValue("com.unity.modules.cloth", "1.0.0"),
    new PackageValue("com.unity.modules.director", "1.0.0"),
    new PackageValue("com.unity.modules.imageconversion", "1.0.0"),
    new PackageValue("com.unity.modules.imgui", "1.0.0"),
    new PackageValue("com.unity.modules.jsonserialize", "1.0.0"),
    new PackageValue("com.unity.modules.particlesystem", "1.0.0"),
    new PackageValue("com.unity.modules.physics", "1.0.0"),
    new PackageValue("com.unity.modules.screencapture", "1.0.0"),
    new PackageValue("com.unity.modules.tilemap", "1.0.0"),
    new PackageValue("com.unity.modules.ui", "1.0.0"),
    new PackageValue("com.unity.modules.uielements", "1.0.0"),
    new PackageValue("com.unity.modules.umbra", "1.0.0"),
    new PackageValue("com.unity.modules.unitywebrequest", "1.0.0"),
    new PackageValue("com.unity.modules.unitywebrequestassetbundle", "1.0.0"),
    new PackageValue("com.unity.modules.unitywebrequestaudio", "1.0.0"),
    new PackageValue("com.unity.modules.unitywebrequesttexture", "1.0.0"),
    new PackageValue("com.unity.modules.unitywebrequestwww", "1.0.0"),
    new PackageValue("com.unity.modules.video", "1.0.0"),
    new PackageValue("com.unity.modules.vr", "1.0.0"),
    new PackageValue("com.unity.modules.wind", "1.0.0"),
    new PackageValue("com.unity.modules.xr", "1.0.0"),
    //YOUT PACKAGE
    new PackageValue("be.eloistree.unitypackagefacilitator", "https://gitlab.com/eloistree/2019_07_21_UnityPackageFacilitator.git"),
    new PackageValue("be.eloistree.quickgitutility","https://gitlab.com/eloistree/2019_07_21_QuickGitUtility.git")
    };

 
   
    private  static string GetPackageFile()
    {
        string result = "";
        result += "\n{";
        result += "\n   \"dependencies\": {";
        for (int i = 0; i < m_wantedDependencies.Length; i++)
        {
            result += string.Format("\n     \"{0}\":\"{1}\"{2} ", m_wantedDependencies[i].m_id, m_wantedDependencies[i].m_version, i < m_wantedDependencies.Length - 1 ? ',' : ' ');
        }
        result += "\n    }  ";
        result += "\n}";
        return result;
    }
    public class PackageValue
    {
        public string m_id = "";
        public string m_version = "0.0.1";

        public PackageValue(string id, string version)
        {
            this.m_id = id;
            this.m_version = version;
        }
    }


    #region GRENADE SCRIPT
    public static bool m_deleteAfterUse = false;
    [UnityEditor.Callbacks.DidReloadScripts]
    private static void OnScriptsReloaded()
    {
        if (IsFileAtProjectRoot())
        {
            Debug.Log("> BOOM : " + GetFileName());
            WhatToDoWhenDrop();
            if (m_deleteAfterUse)
            {
                File.Delete(GetFilePath());
            }
            else
            {
                string filePath = GetFilePath();
                string dir = Application.dataPath + "/GrenadeScripts/Editor/Used";
                Directory.CreateDirectory(dir);
                File.Move(filePath, dir + "/" + GetFileName());
                AssetDatabase.Refresh();
            }
        }
    }
    private static bool IsFileAtProjectRoot()
    {
        return File.Exists(Application.dataPath + "/" + GetFileName());
    }

    private static string GetFileName()
    {
        return Path.GetFileName(new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName());
    }
    private static string GetFilePath()
    {
        return new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
    }
    #endregion
}
