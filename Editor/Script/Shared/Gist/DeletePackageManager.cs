﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class DeletePackageManager : MonoBehaviour
{
    public static void WhatToDoWhenDrop()
    {
        if (File.Exists(Application.dataPath + "/../Packages/manifest.json"))
        {
            string packagePath = Application.dataPath + "/../Packages/manifest.json";
            if (File.Exists(packagePath))
                File.Delete(packagePath);
            AssetDatabase.Refresh();
        }
    }

    #region GRENADE SCRIPT
    public static bool m_deleteAfterUse = false;
    [UnityEditor.Callbacks.DidReloadScripts]
    private static void OnScriptsReloaded()
    {
        if (IsFileAtProjectRoot())
        {
            Debug.Log("> BOOM : " + GetFileName());
            WhatToDoWhenDrop();
            if (m_deleteAfterUse)
            {
                File.Delete(GetFilePath());
            }
            else
            {
                string filePath = GetFilePath();
                string dir = Application.dataPath + "/GrenadeScripts/Editor/Used";
                Directory.CreateDirectory(dir);
                File.Move(filePath, dir + "/" + GetFileName());
                AssetDatabase.Refresh();
            }
        }
    }
    private static bool IsFileAtProjectRoot()
    {
        return File.Exists(Application.dataPath + "/" + GetFileName());
    }

    private static string GetFileName()
    {
        return Path.GetFileName(new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName());
    }
    private static string GetFilePath()
    {
        return new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
    }
    #endregion
}